package ru.tsc.kyurinova.tm.api.service;

import ru.tsc.kyurinova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
