package ru.tsc.kyurinova.tm.repository;

import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> listOfTasks = new ArrayList<>(tasks);
        listOfTasks.sort(comparator);
        return listOfTasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findById(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task startById(String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public boolean existsById(final String id) {
        final Task task = findById(id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (index < 0) return false;
        return index < tasks.size();
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskToProjectById(String projectId, String taskId) {
        final Task task = findByProjectAndTaskId(projectId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public Task findByProjectAndTaskId(String projectId, String taskId) {
        for (Task task : tasks) {
            if (taskId.equals(task.getId()) && projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(projectId);
        for (Task task : listByProject) {
            tasks.remove(task);
        }
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }
}
